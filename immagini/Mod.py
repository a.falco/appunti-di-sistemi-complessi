import numpy as np 
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors

x=[]; y=[]
N=9
a=0.25678
x.append(a%1); y.append(0)
for i in range (N):
    y.append((x[i]*2.0)%1)
    x.append((y[i+1]*2.0)%1)
    plt.plot([x[i],x[i]],[y[i],y[i+1]],color='royalblue')
    plt.plot([x[i],x[i+1]],[y[i+1],y[i+1]],color='royalblue')
plt.show()
#print(x)
#print(y)
