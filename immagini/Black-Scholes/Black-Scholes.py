#!/usr/bin/env python3
import numpy as np 
import matplotlib.pyplot as plt
import scipy.integrate as integrate


xx = np.linspace(0.01,2,10000)      #S/K
N1 = []
N2 = []
#Parametri
Deltat=1        #T-tau
r=0.1
sigma=0.2
kb=(2*r)/sigma**2
d1=(np.log(xx)+(r+(sigma**2)/2)*(Deltat))/(sigma*np.sqrt(Deltat))
d2=d1-sigma*np.sqrt(Deltat)
#print(d1[5])
for i in range(len(xx)):
    N1.append(integrate.quad(lambda x: np.exp(-x**2/2)/(2*np.pi), -np.inf, d1[i]))
for i in range(len(xx)):
    N2.append(integrate.quad(lambda x: np.exp(-x**2/2)/(2*np.pi), -np.inf, d2[i]))
N1=np.array(N1)
N2=np.array(N2)


#C(S,t)/K
yy = N1-np.exp(-r*Deltat)*N2
zz = N1
'''
plt.plot(xx, yy)
plt.plot(xx, zz)
plt.grid()
plt.legend()
plt.show()
'''
fig, ax = plt.subplots()
ax.plot(xx, yy, label='C/K')
ax.plot(xx, zz, label='Delta')
legend = ax.legend(loc='upper center', shadow=True)
plt.grid()
plt.show()
