#!/usr/bin/env python3
import numpy as np 
import matplotlib.pyplot as plt

x = np.linspace(1.0001,5,10000)
y = 50*((1./(2*np.pi))**2)*(1/((x-1)**(3/2)))*np.exp(-1/(2*(x-1)))

plt.plot(x, y)
plt.xlim(0.3,5.3)
plt.grid()
plt.show()
