import numpy as np 
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors
import imageio


def standardmap(l,n,x0):
    #l=0.6   #lambda value
    xx = np.linspace(0,1,10000)
    yy = 4*l*xx*(1-xx)

    #n=9
    #x0=0.25678
    x=[]; y=[]
    x.append(x0); y.append(x0)
    for i in range (n):
        y.append(4*l*x[i]*(1-x[i]))
        x.append(4*l*y[i]*(1-y[i]))
        plt.plot([x[i],x[i]],[y[i],y[i+1]],color='C2')
        plt.plot([x[i],x[i+1]],[y[i+1],y[i+1]],color='C2')

    plt.plot(xx, yy)
    plt.plot(xx, xx, color='C1')
    plt.xlim(0,1)
    plt.grid()
    #plt.show()
    plt.savefig('./Standard/{:.2f}.png'.format(l)) #l*100
    plt.clf()   #pulisce tutta la figura così non si sovrascrive


#GIF
for ll in range (30,100):
    standardmap(ll/100,100,0.25678)

images = []
for l in range (30,100):
    images.append(imageio.imread('./Standard/{:.2f}.png'.format(l/100)))
imageio.mimsave('Standard.gif', images)
