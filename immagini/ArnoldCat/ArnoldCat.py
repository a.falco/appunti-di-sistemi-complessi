import numpy as np 
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors
import matplotlib.cm as cm
import imageio

Ndots=100
angolo=np.linspace(0,2*np.pi,Ndots)
r=0.1   #raggio
cx=0.7; cy=0.7 #centro
x=[];y=[]
for i in range(Ndots):
    x.append(cx+r*np.cos(angolo[i]))
    y.append(cx+r*np.sin(angolo[i]))

colors = cm.rainbow(np.linspace(0, 1, Ndots))
for i, j in zip(np.arange(Ndots), colors):
    plt.scatter(x[i], y[i], s=5, color=j)
plt.xlim(0,1)
plt.ylim(0,1)
plt.title('N=%d' %(0))
plt.grid()
#plt.show()
plt.savefig('./Standard/{:03d}.png'.format(0)) #l*100
plt.clf()

iterazioni=300
for ll in range(iterazioni):
    for k in range(Ndots):
        x[k]=(x[k]+y[k])%1
        y[k]=(x[k]+2*y[k])%1
    colors = cm.rainbow(np.linspace(0, 1, Ndots))
    for i, j in zip(np.arange(Ndots), colors):
        plt.scatter(x[i], y[i], s=5, color=j)
    plt.xlim(0,1)
    plt.ylim(0,1)
    plt.title('N=%d' %(ll+1))
    plt.grid()
    #plt.show()
    plt.savefig('./Standard/{:03d}.png'.format(ll+1)) #l*100
    plt.clf()   #pulisce tutta la figura così non si sovrascrive
'''
plt.scatter(x, y, s=5)
plt.xlim(0,1)
plt.ylim(0,1)
plt.show()
'''
'''
def standardmap(l,n,x0):
    #l=0.6   #lambda value
    xx = np.linspace(0,1,10000)
    yy = 4*l*xx*(1-xx)

    #n=9
    #x0=0.25678
    x=[]; y=[]
    x.append(x0); y.append(x0)
    for i in range (n):
        y.append(4*l*x[i]*(1-x[i]))
        x.append(4*l*y[i]*(1-y[i]))
        plt.plot([x[i],x[i]],[y[i],y[i+1]],color='C2')
        plt.plot([x[i],x[i+1]],[y[i+1],y[i+1]],color='C2')

    plt.plot(xx, yy)
    plt.plot(xx, xx, color='C1')
    plt.xlim(0,1)
    plt.grid()
    #plt.show()
    plt.savefig('./Standard/{:.2f}.png'.format(l)) #l*100
    plt.clf()   #pulisce tutta la figura così non si sovrascrive


#GIF
for ll in range (30,100):
    standardmap(ll/100,100,0.25678)

images = []
for l in range (30,100):
    images.append(imageio.imread('./Standard/{:.2f}.png'.format(l/100)))
imageio.mimsave('Standard.gif', images)
'''
