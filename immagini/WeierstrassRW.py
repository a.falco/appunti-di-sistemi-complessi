import numpy as np 
import matplotlib.pyplot as plt
from random import seed
from random import random
from random import randrange

x=[]; y=[]
M=0.5; b=2
seed(38)     #seed belli con passi=100: 38,48,51,64,65,77,83; con passi=1000: 48
for i in range (100):
    rnd=[random(),random()]
    sgn=randrange(2)
    j=0
    while rnd[0]<M**j:
        j=j+1
    x.append(((-1)**sgn)*b**(j-1))
    x.append(0)
    j=0
    while rnd[1]<M**j:
        j=j+1
    y.append(0)
    y.append(((-1)**sgn)*b**(j-1))
print([max(x),min(x)])
print([max(y),min(y)])
x=np.cumsum(x)
y=np.cumsum(y)
plt.plot(x, y)
#plt.grid()
#plt.title('sees=%d'%s)
plt.show()
'''
for s in range(100):
    x=[]; y=[]
    M=0.5; b=2
    seed(s)     #seed belli: 2, 2.1, 2.2, 7, 10
    for i in range (100):
        rnd=[random(),random()]
        sgn=randrange(2)
        j=0
        while rnd[0]<M**j:
            j=j+1
        x.append(((-1)**sgn)*b**(j-1))
        x.append(0)
        j=0
        while rnd[1]<M**j:
            j=j+1
        y.append(0)
        y.append(((-1)**sgn)*b**(j-1))
    print([max(x),min(x)])
    print([max(y),min(y)])
    x=np.cumsum(x)
    y=np.cumsum(y)
    plt.plot(x, y)
    #plt.grid()
    plt.title('sees=%d'%s)
    plt.show()
'''
