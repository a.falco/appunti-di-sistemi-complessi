import numpy as np 
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors


l=0.77   #lambda value
xx = np.linspace(0,1,10000)
yy = 4*l*xx*(1-xx)
zz = 4*l*(4*l*xx*(1-xx))*(1-(4*l*xx*(1-xx)))
'''
n=9
x0=0.25678
x=[]; y=[]
x.append(x0); y.append(x0)
for i in range (n):
    y.append(4*l*x[i]*(1-x[i]))
    x.append(4*l*y[i]*(1-y[i]))
    plt.plot([x[i],x[i]],[y[i],y[i+1]],color='C2')
    plt.plot([x[i],x[i+1]],[y[i+1],y[i+1]],color='C2')
'''
plt.plot(xx, yy)
plt.plot(xx, xx, color='C1')
plt.plot(xx, zz, color='C9')
plt.xlim(0,1)
plt.ylim(0,l+0.05)
plt.grid()
plt.show()

