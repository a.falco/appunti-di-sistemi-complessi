# Appunti di SC

[![build status](https://gitlab.com/a.falco/appunti-di-sistemi-complessi/badges/master/pipeline.svg)](https://gitlab.com/a.falco/appunti-di-sistemi-complessi/-/jobs/artifacts/master/raw/AppuntiSC.pdf?job=build)

Appunti del corso di Sistemi Complessi tenuto all'Università di Pisa dal Prof. Riccardo Mannella durante l'a.a. 2020-2021. L'ultimo pdf è scaricabile [qui](https://gitlab.com/a.falco/appunti-di-sistemi-complessi/-/jobs/artifacts/master/raw/AppuntiSC.pdf?job=build).
